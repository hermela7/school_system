package school;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Student {
	private final String id;
	private final String name;
	private final String fathersName;
	private final Grade grade;
	private final Section section;
	private final Gender sex;
	private final String DateOfBirth;
	
	
	public static enum Grade {
		NURSERY, KG1, KG2, ONE, TWO, THEREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, ELEVEN, TWELVE
	}
	
	public static enum Gender {
		MALE, FEMALE
	}
	public static enum Section {
		A, B, C, D, E, F
	}
}
