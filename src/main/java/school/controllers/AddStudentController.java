package school.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addStudent")
public class AddStudentController {
	@GetMapping
	public String addStudent() {
	  return "addStudent";    
	}
}
